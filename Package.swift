import PackageDescription

let package = Package(
    name: "tuya",
    targets: [Target(name: "tuya", dependencies: [])],
    dependencies: [
        .Package(url: "https://github.com/thii/SwiftHEXColors.git", majorVersion: 1),
        .Package(url: "https://github.com/Alamofire/Alamofire.git", majorVersion: 4),
        .Package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", versions: Version(1,0,0)..<Version(3, .max, .max)),
        .Package(url: "https://github.com/onevcat/Kingfisher.git",
                 majorVersion: 3),
    ]
)
