//
//  AppDelegate.swift
//  tuya
//
//  Created by huoding on 2017/3/8.
//  Copyright © 2017年 huoding. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import SwiftHEXColors

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window?.backgroundColor = UIColor.white
        window?.rootViewController?.view.isHidden = true
        let manager = NetworkReachabilityManager(host: "www.poocg.me")
        
        manager?.listener = { status in
            print("Network Status Changed: \(status)")
            switch status {
            case .reachable(_):
                self.initMainConfig()
            default: break
                
            }
        }
        
        manager?.startListening()
        
        ShareSDK.registerApp("52c4cf4117e0", activePlatforms:[
            SSDKPlatformType.typeSinaWeibo.rawValue,
            SSDKPlatformType.subTypeWechatSession.rawValue,
            SSDKPlatformType.subTypeWechatTimeline.rawValue,
            SSDKPlatformType.typeQQ.rawValue],
                             onImport: { (platform : SSDKPlatformType) in
                                switch platform
                                {
                                case SSDKPlatformType.typeSinaWeibo:
                                    ShareSDKConnector.connectWeibo(WeiboSDK.classForCoder())
                                case SSDKPlatformType.typeWechat:
                                    ShareSDKConnector.connectWeChat(WXApi.classForCoder())
                                case SSDKPlatformType.typeQQ:
                                    ShareSDKConnector.connectQQ(QQApiInterface.classForCoder(), tencentOAuthClass: TencentOAuth.classForCoder())
                                default:
                                    break
                                }
                                
        }) { (platform : SSDKPlatformType, appInfo : NSMutableDictionary?) in
            
            switch platform
            {
            case SSDKPlatformType.typeSinaWeibo:
                //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                appInfo?.ssdkSetupSinaWeibo(byAppKey: "1106466302",
                                            appSecret : "dd5336a25e1e446600725a2effac3b8f",
                                            redirectUri : "http://www.poocg.com",
                                            authType : SSDKAuthTypeBoth)
                
            case SSDKPlatformType.typeWechat:
                //设置微信应用信息
                appInfo?.ssdkSetupWeChat(byAppId: "wxcee3b015f949e4af", appSecret: "b6d22f76305e35e146af5bb6c7e7101b")

            case SSDKPlatformType.typeQQ:
                //设置QQ应用信息
                appInfo?.ssdkSetupQQ(byAppId: "101142608",
                                     appKey : "a447746567fae56f952cdaf4f393a030",
                                     authType : SSDKAuthTypeBoth)
            default:
                break
            }
            
        }
        
        
        return true
    }
    
    func initMainConfig() {
        Alamofire.request("https://www.poocg.me/webapp/mainjson.php?platform=ios").responseJSON { response in
            switch response.result {
            case .success(let value):
                UserDefaults.standard.set(value, forKey: "mainjson")
                self.processConfig(value as! Dictionary<String, Any>)
            case .failure(let error):
                print(error)
                UserDefaults.standard.object(forKey: "mainjson")
            }
        }
    }
    
    func processConfig(_ value : Dictionary<String, Any>) {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let tabBarVC = self.window?.rootViewController as! UITabBarController
        var vcArray = [UINavigationController]()
        
        let json = JSON(value)
        print("JSON: \(json)")
        let color = UIColor(hexString: json["color"].stringValue)
        let selectedColor = UIColor(hexString: json["selectedColor"].stringValue)
        if #available(iOS 10.0, *) {
            tabBarVC.tabBar.unselectedItemTintColor = color
        } else {
            // Fallback on earlier versions
        }
        tabBarVC.tabBar.tintColor = selectedColor
        tabBarVC.tabBar.backgroundImage = UIImage(named: "qq.png")
        tabBarVC.tabBar.shadowImage = UIImage(named: "qq.png")

        let tabs = json["tabBar"].arrayValue

        
        for i in 0 ..< tabs.count {
            let iconUrl = URL(string: tabs[i]["iconPath"].stringValue)
            let selectIconUrl = URL(string: tabs[i]["selectedIconPath"].stringValue)
            
            let iconResource = ImageResource(downloadURL: iconUrl!, cacheKey: iconUrl?.absoluteString)
            let navC = storyBoard.instantiateViewController(withIdentifier: "FirstScene") as! UINavigationController
            let selectIconResource = ImageResource(downloadURL: selectIconUrl!, cacheKey: iconUrl?.absoluteString)
            let vc = storyBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            vc.request = URLRequest(url: URL(string: tabs[i]["pagePath"].stringValue)!)
            navC.viewControllers = [vc]
            navC.navigationBar.setBackgroundImage(UIImage(named: "qq.png"), for: .default)
            navC.navigationBar.shadowImage = UIImage(named: "qq.png")
            KingfisherManager.shared.retrieveImage(with: iconResource, options: [.scaleFactor(2)], progressBlock: nil, completionHandler: { (img, error, cacheType, url) in
                let newImg = img?.kf.resize(to: CGSize(width:36,height:36))
                navC.tabBarItem = UITabBarItem(title: tabs[i]["text"].stringValue, image: newImg, tag: i+100)
            })
            KingfisherManager.shared.retrieveImage(with: selectIconResource, options: [.scaleFactor(2)], progressBlock: nil, completionHandler: { (img, error, cacheType, url) in
                navC.tabBarItem.selectedImage =  img?.kf.resize(to: CGSize(width:36,height:36))
            })
            vcArray.append(navC)
        }
        tabBarVC.viewControllers = vcArray
        tabBarVC.tabBar.isHidden = false
        window?.rootViewController?.view.isHidden = false

    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

