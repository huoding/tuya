//
//  FirstViewController.swift
//  tuya
//
//  Created by huoding on 2017/3/8.
//  Copyright © 2017年 huoding. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import SKPhotoBrowser

class FirstViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var request: URLRequest? = nil
    @IBOutlet weak var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if request == nil {
            request = URLRequest(url: URL(string: "https://www.poocg.me/webapp.php")!)
        }
        let backBarItem = UIBarButtonItem(image: UIImage(named: "left.png"), style: .done, target: self, action: #selector(FirstViewController.back))
        if (self.navigationController?.viewControllers.count)! > 2 {
            let closeBarItem = UIBarButtonItem(image: UIImage(named: "close.png"), style: .done, target: self, action: #selector(FirstViewController.close))
            self.navigationItem.leftBarButtonItems = [backBarItem,closeBarItem]
        } else if (self.navigationController?.viewControllers.count)! == 2{
            self.navigationItem.leftBarButtonItem = backBarItem
        } else {
            self.navigationItem.leftBarButtonItem = nil
        }
        webview.loadRequest(request!)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.refresh), name: NSNotification.Name(rawValue: "refresh_webview"), object: nil)
//        let singleTap = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.doTap))
//        singleTap.delegate = self
//        webview.addGestureRecognizer(singleTap)
    }
    func close() {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func refresh() {
        webview.loadRequest(request!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    func doTap(sender: UITapGestureRecognizer) {
//        let pt = sender.location(in: self.webview.scrollView)
//        let js = String(format: "document.elementFromPoint(%f, %f)", pt.x, pt.y)
//        let target = webview.stringByEvaluatingJavaScript(from: js)
//        print(target ?? "no")
//    }
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
}

extension FirstViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if (request.url?.absoluteString.hasPrefix("push://"))! {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            vc.request = request
            vc.hidesBottomBarWhenPushed = true
            vc.request?.url = URL(string: (request.url?.absoluteString.replacingOccurrences(of: "push://", with: "https://"))!)
            self.navigationController?.pushViewController(vc, animated: true)
            print(request)
            return false
        } else if (request.url?.absoluteString.hasPrefix("pop://"))! {
            if (request.url?.absoluteString.contains("refresh"))! {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh_webview"), object: nil)
            }
            if (self.presentingViewController != nil) {
                self.dismiss(animated: true, completion: nil)
            }else {
                _ = self.navigationController?.popViewController(animated: true)
            }
            print(request)
            return false
        } else if (request.url?.absoluteString.hasPrefix("reset://"))! {
            _ = self.navigationController?.popToRootViewController(animated: true)
            print(request)
            return false
        }else if (request.url?.absoluteString.hasPrefix("refresh://"))! {
            if (request.url?.absoluteString.lengthOfBytes(using: .utf8))! > 10 {
                var reRequset = request
                reRequset.url = URL(string: (request.url?.absoluteString.replacingOccurrences(of: "refresh://", with: "https://"))!)
                self.request = reRequset
                webview.loadRequest(reRequset)
            } else {
                webview.loadRequest(self.request!)
            }
            print(request)
            return true
        }else if (request.url?.absoluteString.hasPrefix("modal://"))! {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            vc.request = request
            vc.request?.url = URL(string: (request.url?.absoluteString.replacingOccurrences(of: "modal://", with: "https://"))!)
            let navC = UINavigationController(rootViewController: vc)
            navC.navigationBar.setBackgroundImage(UIImage(named: "qq.png"), for: .default)
            navC.navigationBar.shadowImage = UIImage(named: "qq.png")
            self.navigationController?.present(navC, animated: true, completion: {
                
            })
            print(request)
            return false
        }else if (request.url?.absoluteString.hasPrefix("share://"))! {
            let str = request.url?.absoluteString.replacingOccurrences(of: "share://", with: "")
            let data = Data(base64Encoded: str!)
            let json = JSON(data!)

            self.shareSNS(image: json["image"].stringValue, url: json["url"].stringValue, title: json["title"].stringValue, content: json["content"].stringValue)
            return false
        }else if (request.url?.absoluteString.hasPrefix("safari://"))! {
            let url =  URL(string: (request.url?.absoluteString.replacingOccurrences(of: "safari://", with: "https://"))!)
            UIApplication.shared.openURL(url!)
            return false
        }else if (request.url?.absoluteString.hasPrefix("photo://"))! {
            let url =  URL(string: (request.url?.absoluteString.replacingOccurrences(of: "photo://", with: "https://"))!)
            Alamofire.request(url!).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let items = json["result"].arrayValue
                    let images = items.map({ (item: JSON) -> SKPhoto in
                        return SKPhoto.photoWithImageURL(item["pic"].stringValue)
                    })
                    let browser = SKPhotoBrowser(photos: images)
                    browser.initializePageIndex(0)
                    self.present(browser, animated: true, completion: {})
                    
                case .failure(let error):
                    print(error)
                    UserDefaults.standard.object(forKey: "mainjson")
                }
            }
            return false
        }

    



        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        indicatorView.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        indicatorView.stopAnimating()
        self.navigationItem.title = webView.stringByEvaluatingJavaScript(from: "document.title")
    }
    func shareSNS(image: String, url: String, title: String, content: String) {
        let shareParames = NSMutableDictionary()
        shareParames.ssdkSetupShareParams(byText: content,
                                          images : image,
                                          url : NSURL(string:url) as URL!,
                                          title : title,
                                          type : SSDKContentType.webPage)
        
        ShareSDK.showShareActionSheet(nil, items: nil, shareParams: shareParames) { (state : SSDKResponseState, platformType:SSDKPlatformType, userData:[AnyHashable : Any]?, entity : SSDKContentEntity?, error :Error?, end:Bool) in
            switch state{
                
            case SSDKResponseState.success: print("分享成功")
            case SSDKResponseState.fail:    print("授权失败,错误描述:\(String(describing: error))")
            case SSDKResponseState.cancel:  print("操作取消")
                
            default:
                break
            }

        }
        
    }
}
